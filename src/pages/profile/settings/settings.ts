import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
import { AppLanguages } from '../../../providers/app-languages';
import { Language } from '../../../providers/app.model';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  isChecked: boolean = true;
  notif: string = "true";
  isBooleanforLive: boolean;
  isBooleanforDash: boolean;
  isBooleanforVehList: boolean;
  mapKey: any;
  fuelKey: string;
  lankey: string;
  islogin: any;
  fuels: any[] = [];
  maps: string[] = [];
  selectedMapKey: string;
  lankeyname: string;
  languages: Language[];
  nightMode: boolean;
  trafficMode: boolean;
  vehName: boolean = true;
  measurementType: string = 'MKS';

  constructor(
    public events: Events,
    private tts: TextToSpeech,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private apiCall: ApiServiceProvider,
    public translate: TranslateService,
    private appLang: AppLanguages) {
    this.fuels = ['LITRE', 'PERCENTAGE'];
    this.maps = [this.translate.instant('Normal'), this.translate.instant('Terrain'), this.translate.instant('Satellite')];
    this.languages = [...this.appLang.Languages];

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));

    if (this.islogin.fuel_unit === 'LITRE') {
      this.fuelKey = 'LITRE';
    } else {
      this.fuelKey = 'PERCENTAGE';
    }

    if (localStorage.getItem('MAP_KEY') != null) {
      this.selectedMapKey = localStorage.getItem('MAP_KEY');
    }
    // if (localStorage.getItem("notifValue") != null) {
    //   this.notif = localStorage.getItem("notifValue");
    //   if (this.notif == 'true') {
    //     this.isChecked = true;
    //   } else {
    //     this.isChecked = false;
    //   }
    // }

    if (localStorage.getItem('NightMode') != null) {
      if (localStorage.getItem('NightMode') === 'ON') {
        this.nightMode = true;
      }
    }

    if (localStorage.getItem('TrafficMode') != null) {
      if (localStorage.getItem('TrafficMode') === 'ON') {
        this.trafficMode = true;
      }
    }

    if (localStorage.getItem('DisplayVehicleName') != null) {
      if (localStorage.getItem('DisplayVehicleName') === 'OFF') {
        this.vehName = false;
      }
    }
  }
  ngOnInit() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.language_code !== undefined) {
          this.lankeyname = resp.language_code;
          this.lankey = resp.language_code;
        }
        if (resp.voice_alert !== undefined) {
          this.notif = resp.voice_alert;
          if (this.notif == 'true') {
            this.isChecked = true;
          } else {
            this.isChecked = false;
          }
        }

        if (resp.unit_measurement !== undefined) {
          this.measurementType = resp.unit_measurement;
          // this.measurementType = resp.unit_measurement;
          if (this.measurementType === 'FPS') {
            if (this.islogin.fuel_unit === 'LITRE') {
              this.fuelKey = 'GALLON';
            } else {
              this.fuelKey = 'PERCENTAGE';
            }
          }
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementType = measureType;
          } else {
            this.measurementType = 'MKS';
          }
        }

      },
        err => {
          console.log(err);
        });

    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") == 'live') {
        this.isBooleanforLive = true;
        this.isBooleanforDash = false;
      } else {
        if (localStorage.getItem("SCREEN") == 'dashboard') {
          this.isBooleanforDash = true;
          this.isBooleanforLive = false;
        }
      }
    }
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter SettingsPage');
  }

  onChangeMeasure(key) {
    console.log(key)
    localStorage.setItem('MeasurementType', key);
    var payload = {
      uid: this.islogin._id,
      unit_measurement: key
    }
    // this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + JSON.stringify(resp));
        // this.apiCall.stopLoading();
        // let toast = this.toastCtrl.create({
        //   message: 'Settings updated!',
        //   duration: 1500,
        //   position: 'bottom'
        // });
        // toast.present();
      },
        err => {
          // this.apiCall.stopLoading();
          console.log(err);
        });
  }

  setLanguage(key) {
    var payload = {
      uid: this.islogin._id,
      lang: key
    }
    // this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + JSON.stringify(resp));
        localStorage.setItem('LanguageKey', key);
        // this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      },
        err => {
          localStorage.setItem('LanguageKey', key);
          // this.apiCall.stopLoading();
          console.log(err);
        });
    this.events.publish('lang:key', key);
    localStorage.setItem('LanguageKey', key);

  }

  setNotif(notif) {
    this.isChecked = notif;
    // if (notif === true) {
    var payload = {
      uid: this.islogin._id,
      voice_alert: notif
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + JSON.stringify(resp));
        this.apiCall.stopLoading();
        let msg;
        if (notif === true) {
          // msg = 'You have succesfully enabled voice notifications';
          msg = this.translate.instant('You have succesfully enabled voice notifications');
        } else {
          msg = this.translate.instant('You have succesfully disabled voice notifications');
          // msg = 'You have succesfully disabled voice notifications';
        }
        this.tts.speak(msg)
          .then(() => { })
          .catch((reason: any) => console.log(reason));
        let toast = this.toastCtrl.create({
          message: this.translate.instant("You have succesfully enabled voice notifications"),
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error occured: ", err)
        });
    // }
  }

  rootpage() {
    let alert = this.alertCtrl.create();
    alert.setSubTitle('Choose default screen');
    alert.addInput({
      type: 'radio',
      label: 'Dashboard',
      value: 'dashboard',
      checked: (this.isBooleanforDash ? this.isBooleanforDash : true)
    });

    alert.addInput({
      type: 'radio',
      label: 'Live Tracking',
      value: 'live',
      checked: this.isBooleanforLive
    });

    alert.addInput({
      type: 'radio',
      label: 'Vehicle List',
      value: 'vehiclelist',
      checked: this.isBooleanforVehList
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data)
        localStorage.setItem("SCREEN", data);
        const toast = this.toastCtrl.create({
          message: 'Default page set to ' + data + ' page',
          duration: 2000
        });
        toast.onDidDismiss(() => {
          this.events.publish("Screen:Changed", data);
        })
        toast.present();
      }
    });
    alert.present();
  }

  onChangeMap(key) {
    console.log("map key changed: ", key)
    localStorage.setItem("MAP_KEY", key);
  }

  onChangeFuel(key) {
    console.log("key changed: ", key)
    const newContact = {
      fname: this.islogin.fn,
      lname: this.islogin.ln,
      org: this.islogin._orgName,
      uid: this.islogin._id,
      fuel_unit: key
    }

    var _baseURl = this.apiCall.mainUrl + "users/Account_Edit";
    this.apiCall.urlpasseswithdata(_baseURl, newContact)
      .subscribe(data => {
        console.log("got response data: ", data)
        var logindetails = JSON.parse(JSON.stringify(data));
        var userDetails = window.atob(logindetails.token.split('.')[1]);
        var details = JSON.parse(userDetails);
        localStorage.setItem('details', JSON.stringify(details));
      })
  }

  showAlert() {
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Set Immobilize Password'),
      message: this.translate.instant("Enter password for engine cut"),
      inputs: [
        {
          name: 'password',
          placeholder: this.translate.instant('Password')
        },
        {
          name: 'cpassword',
          placeholder: this.translate.instant('Confirm Password')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('SAVE'),
          handler: data => {
            console.log('Saved clicked');
            if (data.password !== data.cpassword) {
              this.toastmsg(this.translate.instant('Entered password and confirm password did not match.'))
              return;
            }
            this.setPassword(data);
          }
        }
      ]
    });
    prompt.present();
  }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  setPassword(data) {
    var payload = {
      uid: this.islogin._id,
      engine_cut_psd: data.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + JSON.stringify(resp));
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant("Immobilize password set successfully"),
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      });
  }

  update(ev) {
    console.log("invoking notification: ", ev);
    if (ev === true) {
      localStorage.setItem('NightMode', 'ON');
    } else {
      localStorage.setItem('NightMode', 'OFF');
    }
  }

  trafficUpdate(ev) {
    console.log("invoking notification for traffic: ", ev);
    if (ev === true) {
      localStorage.setItem('TrafficMode', 'ON');
    } else {
      localStorage.setItem('TrafficMode', 'OFF');
    }
  }

  vehNameUpdate(ev) {
    if (ev == false) {
      let alert = this.alertCtrl.create({
        message: this.translate.instant("Are you sure, you don't want to display vehicle names on multiple live screen?"),
        buttons: [
          {
            text: this.translate.instant('Back'),
            handler: () => {
              this.vehName = true;
            }
          },
          {
            text: this.translate.instant('Yes'),
            handler: () => {
              localStorage.setItem('DisplayVehicleName', 'OFF');
            }
          }
        ]
      });
      alert.present();
    } else {
      localStorage.setItem('DisplayVehicleName', 'ON');
    }
  }

  notifSet() {
    this.navCtrl.push('NotifSettingPage');
  }

}
